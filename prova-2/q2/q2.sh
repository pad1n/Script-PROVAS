#!/bin/bash

echo
echo "SCRIPT DE BACKUP"
echo "----------------"
echo

# Definindo origem e destino
a="$HOME"
b="/tmp/backup"

# Criando o diretório de destino, caso o mesmo não exista
mkdir -p "$b"

# Definindo o horário que será usado no nome do backup
c=$(date +"%H%M")

# Escrevendo o backup no formato pedido
d="backup_${c}.tar.gz"

# Destino do backup compactado
e="${b}/${d}"

# Compactando os arquivos de backup
tar -czf "$e" -C "$a" .

echo "Backup completo! O arquivo de backup foi salvo em $e"
