#!/bin/bash

# Função para exibir a interface gráfica e agendar o backup

yad_bkp() {
    	# Exibir a interface gráfica para agendamento
    	a=$(yad --calendar --time --title "Agendamento de Backup" \
                         --text "Selecione a data e a hora para o agendamento do backup:" \
                         --width 300 --height 300 \
                         --form --field "Data e Hora:":DT \
                         --button="Agendar:0" --button="Cancelar:1")    
    	# Verifica se o usuário clicou em "Cancelar"
    	if [[ $? -ne 0 ]]; then
        	echo "Agendamento cancelado."
        	exit 1
    	fi

    	# Obtém a data e hora selecionadas
    	b=$(echo "$date_time" | awk -F '|' '{print $1}')
    
    	# Prepara o comando para o agendamento usando o comando at
    	c="tar -czf /tmp/backup/backup_$(date +'%H%M').tar.gz -C $HOME ."
    
   	 # Agendar o script usando o comando at
    	echo "$c" | at "$b"

    	echo "Backup agendado para: $b."
}

# Executa a função de agendamento
yad_bkp
yad --info --text "Backup agendado para $yad_bkp."

