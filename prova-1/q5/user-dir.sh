#!/bin/bash

echo "**** SCRIPT PARA CRIAR DIRETÓRIOS PARA O USUÁRIO ****"

echo "Digite 4 nomes:"
read -r a
read -r b
read -r c
read -r d

echo "Criando os diretórios..."
echo "Aguarde..."
sleep 0.5
echo -e "Diretórios criados!\n"

mkdir "$a"
mkdir "$b"
mkdir "$c"
mkdir "$d"

e=$(date +"%d-%m-%Y")

echo -e "# $a\n\nCriado em: $e" > "$a/README.md"
echo -e "# $b\n\nCriado em: $e" > "$b/README.md"
echo -e "# $c\n\nCriado em: $e" > "$c/README.md"
echo -e "# $d\n\nCriado em: $e" > "$d/README.md"

echo "Diretórios e arquivos README.md criados com sucesso!"


