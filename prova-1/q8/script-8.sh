#!/bin/bash

echo "Digite o nome de três arquivos:"
read a b c

d=$(wc -l < "$a")
e=$(wc -l < "$b")
f=$(wc -l < "$c")

g=$((d + e + f))

echo "Total de linhas nos arquivos: $g"
