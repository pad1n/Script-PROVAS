#!/bin/bash

a=$(date +%Y-%m-%d)

b=$(date -d "$a + 3 days" +%Y-%m-%d)

c=$(date -d "a + 7 days" +%Y-%m-%d)

echo "Próxima quarta-feira: $a"
echo "Quarta-feira seguinte: $b"
