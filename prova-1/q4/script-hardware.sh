#!/bin/bash

echo -e "Informações sobre o hardware.\n"

echo "**** VERSÃO DO KERNEL ****"
uname -r
read -p "[ENTER] PARA CONTINUAR"

echo "**** CPU ****"
lscpu
read -p "[ENTER] PARA CONTINUAR"

echo "**** INFORMAÇÕES SOBRE MEMÓRIA ****"
free -m
read -p "[ENTER] PARA CONTINUAR"

echo "**** DISPOSITIVOS CONECTADOS ÀS PORTAS PCI ****"
lspci
read -p "[ENTER] PARA CONTINUAR"

echo "**** DISPOSITIVOS CONECTADOS ÀS PORTAS USB ****"
lsusb
read -p "[ENTER] PARA CONTINUAR"

echo "**** INFORMAÇÕES SOBRE PARTIÇÕES, DISCOS RÍDIGOS ****"
lsblk
read -p "[ENTER] PARA FINALIZAR"
