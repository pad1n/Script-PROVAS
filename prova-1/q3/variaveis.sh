#!/bin/bash
#
echo -e "Script feito para explicar todas as formas de criar variáveis no bash\n"
echo -e 'Variáveis definidas pelo usuário: é o tipo de variável em que o usuário atribui o seu valor diretamente. Exemplo: a=10; b="teste"; c=${b}\n'
sleep 0.5

echo -e 'Variáveis de entrada padrão: é o tipo de variável que ler a entrada do usuário e armazena em uma variável. Exemplo: read VARIAVEL\n'
sleep 0.5

echo -e 'Variáveis do sistema: é o tipo de variável que existem na configuração e no ambiente do shell. Exemplo: echo ${PATH}\n'
sleep 0.5

echo 'Variáveis definidas pelo bash: é o tipo de variável pré-definida do bash. Exemplo: $1 $2 $3; $*; $0'
